<?php

namespace A2C\Bundle\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class A2CUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
