<?php

namespace A2C\Bundle\TranslationFormBundle\Locale;


interface RepositoryInterface
{
    public function getAllLocale();
}