<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),

            //A2C Core
            new A2C\Bundle\CoreBundle\A2CCoreBundle(),
            new \Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new A2C\Bundle\MenuBundle\A2CMenuBundle(),
            new A2C\Bundle\LanguageBundle\A2CLanguageBundle(),

            //A2C Security
            new FOS\UserBundle\FOSUserBundle(),
            new A2C\Bundle\UserBundle\A2CUserBundle(),

            // Dashboard
            new A2C\Bundle\DashboardBundle\A2CDashboardBundle(),

            //Pagination
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),

            // ApiKey
            new Uecode\Bundle\ApiKeyBundle\UecodeApiKeyBundle(),

            //Upload Files
            new Oneup\UploaderBundle\OneupUploaderBundle(),
            new Knp\Bundle\GaufretteBundle\KnpGaufretteBundle(),
            new Liip\ImagineBundle\LiipImagineBundle(),
            new Jb\Bundle\FileUploaderBundle\JbFileUploaderBundle(),

            //Aplication
            new AppBundle\AppBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new A2C\Bundle\GeneratorBundle\A2CGeneratorBundle();
            $bundles[] = new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
